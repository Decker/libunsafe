#ifndef wifi_h
#define wifi_h

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>

ESP8266WiFiMulti WiFiMulti;

const uint16_t listeningPort = 8080;

WiFiServer server(listeningPort);

void listenForHttpMsgs(std::function<void(String)> callbackFn) {
    WiFiClient client = server.available();
    if (!client) {
        return;
    }

    // Wait until the client sends some data
    Serial.println("new client");
    while(!client.available()){
        delay(1);
    }

    // Read the first line of the request
    String req = client.readStringUntil('\r');
    Serial.println(req);
    client.flush();

    // Match the request
    int val;
    if (req.indexOf("/flashlight/on") != -1) {
      val = 1;
      callbackFn("on");
    } else if (req.indexOf("/flashlight/off") != -1) {
      val = 0;
      callbackFn("off");
    } else {
      Serial.println("invalid request");
      client.stop();
      return;
    }

    client.flush();

    // Prepare the response
    String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html>\r\nFlashlight is now ";
    s += (val)?"on":"off";
    s += "</html>\n";

    // Send the response to the client
    client.print(s);
    delay(1);
    Serial.println("Client disconnected");
}

void connectToWiFiAP(const char* SSID, const char* PASS) {
  WiFi.hostname("flashlight_transceiver");

  WiFi.begin(SSID, PASS);

  Serial.println();
  Serial.println();
  Serial.print("Wait for WiFi... ");

  while(WiFi.status() != WL_CONNECTED) {
      Serial.print(".");
      delay(500);
  }

  Serial.println("");
  Serial.println("WiFi connected");

  server.begin();

  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

String calculateHealth() {
    int strength = WiFi.RSSI();
    boolean healthy = strength > -70;
    String out = "&healthy=";
    out += String(healthy);
    out += "&msg=";
    out += healthy ? "OK": "Signal strength is low (" + String(strength) + ")";
    return out;
}

String getLocalMac() {
  uint8_t macAddr[6];
  WiFi.macAddress(macAddr);
  char mac[18] = { 0 };
  sprintf(mac, "%02X:%02X:%02X:%02X:%02X:%02X", macAddr[0], macAddr[1], macAddr[2], macAddr[3], macAddr[4], macAddr[5]);
  return String(mac);
}

void reportHealth(String watchDogIp, String deviceId = "") {
  Serial.println("Begin report health");
  Serial.println("Wifi-status: " + String(WiFi.status()) + " " + String(WiFi.RSSI()));

  if (deviceId.length() == 0) {
    deviceId = getLocalMac();
  }

  HTTPClient http;
  http.setTimeout(1000);
  http.begin("http://" + String(watchDogIp) + ":8050/service/report");
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");
  http.POST("from=" + String(deviceId) + calculateHealth());
  http.writeToStream(&Serial);
  http.end();
  Serial.println("Complete report health");
}

void sendSendingStationReport(String brainIp, bool isFirstCode, bool isOk) {
  Serial.println("Begin sending station report");
  Serial.println("Wifi-status: " + String(WiFi.status()) + " " + String(WiFi.RSSI()));

  String status = String(isFirstCode ? "FIRST/" : "SECOND/");
  status += String(isOk == 1 ? "OK" : "NOTOK");
  status += String("?id=") + getLocalMac();

  HTTPClient http;
  http.setTimeout(3000);
  http.begin("http://" + String(brainIp) + ":8080/service/sending-station/" + status);
  http.POST("");
  http.writeToStream(&Serial);
  http.end();
  Serial.println("Complete sending station report");
}

#endif
