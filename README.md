# README #

A networking utility library for reusing code across Unsafe LARP Arduino codebases.

### Setup ###

* Clone library
* Include as third-party library in your Arduino IDE or Platformio project
* Compile and run

### Contact ###

* unsafelarp.wordpress.com